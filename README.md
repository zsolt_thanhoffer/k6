## Contact

**Repository Owner**: Balázs Cziráky (balazs@cziraky.io)

**Repository Maintainers**: Balázs Cziráky (balazs@cziraky.io)

## K6 Load Test

Basic performance testing for HTTP endpoints using the K6 load testing tool. Using this template project you can test your HTTP endpoints via Multi-Project Downstream-Pipelines and get a load test result similar to one below. 

**Note:** *Currently only GET method is supported.*

```bash
$ k6 run --vus 1000 --duration 30s --insecure-skip-tls-verify aws-sls.js 

          /\      |‾‾| /‾‾/   /‾‾/
     /\  /  \     |  |/  /   /  /
    /  \/    \    |     (   /   ‾‾\
   /          \   |  |\  \ |  (‾)  |
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: aws-sls.js
     output: -

  scenarios: (100.00%) 1 scenario, 1000 max VUs, 1m0s max duration (incl. graceful stop):
           * default: 1000 looping VUs for 30s (gracefulStop: 30s)


running (0m30.2s), 0000/1000 VUs, 620859 complete and 0 interrupted iterations
default ✓ [======================================] 1000 VUs  30s

     data_received..............: 183 MB 6.1 MB/s
     data_sent..................: 53 MB  1.8 MB/s
     http_req_blocked...........: avg=1.31ms   min=170ns  med=330ns   max=3.16s    p(90)=470ns   p(95)=520ns
     http_req_connecting........: avg=929.7µs  min=0s     med=0s      max=3.06s    p(90)=0s      p(95)=0s
     http_req_duration..........: avg=46.91ms  min=2.92ms med=43.93ms max=1.89s    p(90)=51.79ms p(95)=55.26ms
     http_req_receiving.........: avg=40.93µs  min=12.9µs med=35.32µs max=49.16ms  p(90)=48.23µs p(95)=55.48µs
     http_req_sending...........: avg=18.1µs   min=5.37µs med=17.18µs max=13.1ms   p(90)=23.21µs p(95)=27.79µs
     http_req_tls_handshaking...: avg=365.93µs min=0s     med=0s      max=909.85ms p(90)=0s      p(95)=0s
     http_req_waiting...........: avg=46.85ms  min=2.88ms med=43.88ms max=1.89s    p(90)=51.73ms p(95)=55.2ms
     http_reqs..................: 620859 20558.942158/s
     iteration_duration.........: avg=48.3ms   min=2.97ms med=44ms    max=3.18s    p(90)=51.88ms p(95)=55.39ms
     iterations.................: 620859 20558.942158/s
     vus........................: 1000   min=1000 max=1000
     vus_max....................: 1000   min=1000 max=1000
```
## How to Use

1) Create a GitLab CI file named e.g. `.performance.yml` with the contents below and include it in your `.gitlab-ci.yml` file.

```yml
#/verify/.performance.yml

test:load:
  stage: performance                                # This stage must be configured in your projects .gitlab-ci.yml
  variables:
      K6_OPTIONS: --vus 1000 --iterations 10000     # Define your K6 options (OPTIONAL)
      K6_SERVICE_PROTOCOL: https                    # Define your endpoints protocol: http/https
      K6_SERVICE_ENDPOINT: <URL>                    # Define your endpoint url without protocol

  trigger:
    project: 0x422e_education/obuda-university/devops/tools/k6
    strategy: depend

```
**Note:** The most commonly used K6 options are: 

  - `--vus`: Number of Virtual Users to run concurrently
  - `--iterations`: Fixed number of iterations to execute
  - `--duration`: Total duration of test run

It is recommended to use either the `--iterations` or the  `--duration` options. 

2) Include the above CI file into yours: 

```yml
#.gitlab-ci.yml (excerpt)

include:
  - "/verify/.performance.yml"    # Relative path to your ./performance.yml

stages:
  - init
  - versioning
  - domp:integration
  - deploy
  - domp:delivery
  - domp:deployment
  - performance                    # Stage used in the included ./performance.yml
  - domp:release

# ...
```

Thats it. You are done. :zap:
